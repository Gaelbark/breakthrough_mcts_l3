## Projet pour le module d'intelligence artificielle pour les étudiants de L3
Le but de ce projet est d'apprendre à explorer l'arbre de jeu via MCTS.

## Rappel
Pour rappel voici un lien vers les commandes git utiles pour ce module :
https://juliendehos.gitlab.io/posts/env/post20-git.html#forker-un-d%C3%A9p%C3%B4t-existant

## Etape 1 : Installation

Forker ce dépôt.

Les paquets nécessaires sont numpy, pillow et tkinter.

## Etape 1 : Création d'un bot aléatoire

## Etape 2 : Création d'un bot Minmax ou Alpha-Beta
Pour cela, commencez par créer une heuristique. Pour pourrez associer des points à chaque pion en fonction de la distance qui le sépare de l'arrivée.

## Etape 3 : Création d'un bot MCTS vanilla

## Etape 4 : Création d'un bot UCT

## Examen M.Chotard
## Question 1
Les noeuds sont des positions dans le jeu et les arêtes des mouvement

## Question 2
Les noeuds les plus intéressants sont les noeuds avec les plus grandes proba de victoire, cela permet de determiner quels sont les meilleurs coups à jouer.

## Question 3
Cela permet d'éliminer les coups que l'on ne veut pas jouer et de rendre le MinMax plus performant

## Question 4

## Question 5
Pour un jeu comme les échecs ou le tic tac toe, le minmax sera plus efficace puisqu'il ne tombera pas dans des pièges ou l'on gagne en quelque coups, chose que fera le mcts, alors que si l'on prend un jeu comme le go, le mcts sera bien plus efficace du à la profondeur des coups possibles.

## Question 6
Le minmax renvoie une victoire.

## Question 7
Si le nombre de simulations tend vers l'infini MCTS aura des estimations de 1, il gagnera a tous les coups

## Question 8
Deux côtés possède deux rangées de pions, chaque pions peut aller dans ses diagonales et devant lui. il ne peut pas passer sur un pion de sa couleur, le premier a atteindre le bord opposé gagne.

## Question 9
C'est un équilibre ou personne ne peut trouver de meilleures solutions pour gagner, si l'on met deux MCTS face à face, ils atteindront cet équilibre.

