import interface
import numpy as np

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interface.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):
	
	def demande_coup(self):
		coup = self.jeu.plateau.liste_coups_valides(self.couleurval)
		return coup[np.random.randint(len(coup))]

class Minmax(IA):
	
	def demande_coup(self):
		(m, argm) = self.minmax(self.jeu.plateau, self.couleurval, 1)
		return argm
		
	def minmax(self, plateau, couleurval, height):
		if height == 0:
			return (eval(plateau, self.couleurval), None)
		liste_coups = plateau.liste_coups_valides(couleurval)
		if couleurval == self.couleurval:
			m, argm = -np.infty, None
			for c in liste_coups:
				new_plateau = plateau.copie()
				new_plateau.jouer(c,couleurval)
				v, mc = self.minmax(new_plateau, -couleurval, height-1)
				if v > m:
					m = v
					argm = c
		else:
			m, argm = np.infty, None
			for c in liste_coups:
				new_plateau = plateau.copie()
				new_plateau.jouer(c,couleurval)
				v, c = self.minmax(new_plateau, -couleurval, height-1)
				if v < m:
					m = v
					argm = c
		return (m, argm)


"""class AlphaBeta(IA):

	def demande_coup(self):
		pass #you know the drill"""

def eval(plateau, couleurval):
	nmb = 0
	for i in range(plateau.taille):
		for j in range(plateau.taille):
			if plateau.tableau_cases[i][j] == 1:
				nmb += 1
			elif plateau.tableau_cases[i][j] == -1:
				nmb -= 1
	return couleurval*nmb

class MonteCarlo (IA):
    
    def montecarlo(self, plateau, couleurval, nb_simulation=100):
        actions = plateau.listes_coups_valides(couleurval)
        stats=[]
        for c in actions:
            v=0
            for i in range(nb_simulation):
                copie_plateau = plateau.copie()
                copie_plateau.jouer(i, couleurval)
                couleur_current = -couleurval
                while (copie_plateau.check_partie_finie()):
                    listecoups = copie_plateau.liste_coups_valides( couleur_current)
                    couprandom = np.random.randint(len(listecoups))
                    copie_plateau.jouer(couprandom, couleur_current)
                    couleur_current *=-1
                if copie_plateau.gagnant==couleurval:
                    v+=1
            stats.append[v]
                
        return np.argmax(actions[np.argmax(stats)])


class MonteCarloTreeSearch (IA):
	def montecarlotreesearch (self, plateau, couleurval, nb_simulation=100):
		StockCoup = MonteCarlo.montecarlo(self, plateau, couleurval, nb_simulation)	
		